
const axios = require('axios');


const getClima = async (lat, lng) => {
    let response = await axios.get(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lng}&units=metric&appid=6536c088f12f86dac7829349386bbc21`);
     
    if (response.data === undefined || response.data.cod === '400') {
        throw new Error('No se lograron obtener los datos del clima')
    }      

    let resultObjet = { 
        temperatura: response.data.main.temp
    };

    return resultObjet;
}

module.exports = {
    getClima
}