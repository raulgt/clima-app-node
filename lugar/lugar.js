const axios = require('axios');

let baseUrl = `https://maps.googleapis.com/maps/`;

const getLugarLatLng = async (direccion) => {

    let encodeDireccion = encodeURI(direccion);
    let geoLocalizationUrl = `api/geocode/json?address=${encodeDireccion}&key=AIzaSyCLugqDQsjFA_RJ7gFf2SSV_UYIw0-VXTI`;
    let response = await axios.get(baseUrl + geoLocalizationUrl);


    if (response.data.status === 'ZERO_RESULTS') {
        throw new Error(`No hay resultados para la ciudad ${direccion}`);
    }

    let ubication = response.data.results[0];
    let geometry = ubication.geometry.location;

    let resultObjet = {
        direccion: ubication.formatted_address,
        lat: geometry.lat,
        lng:geometry.lng
    }
    return resultObjet;
};

module.exports = {
    getLugarLatLng
}