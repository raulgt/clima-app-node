const argv = require('yargs').options({
    direccion:{
        desc:'Direccion de la ciudad por obtener el clima',
        alias: 'd',
        demand: true
    }
}).argv


module.exports ={
    argv
}