
const argv = require('./config/yargs').argv;

const lugar = require('./lugar/lugar');
const clima = require('./clima/clima');


let getInfoCountry = async (direccion) => {

    try {
        let coord = await lugar.getLugarLatLng(direccion);
        let climas = await clima.getClima(coord.lat, coord.lng);

        return `El clima en ${coord.direccion} es de ${climas.temperatura}°C`;
    } catch (error) {
        return `No se pudo determinar el clima en ${direccion}`;
    }
}

getInfoCountry(argv.direccion)
    .then(mensaje => console.log(mensaje))
    .catch(err => console.log(err));






